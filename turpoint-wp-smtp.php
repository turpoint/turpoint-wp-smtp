<?php

/**
 * Plugin Name: WP SMTP
 * Author: Turpoint
 * Author URI: https://turpoint.com
 * Description: A plugin for sending all e-mails of a WordPress installation via an external SMTP account. Requires ACF to be installed.
 * Version: 1.0.7
 * Requires at least: 5.5
 * Text Domain: turpoint-wp-smtp
 */

require_once(dirname(__FILE__) . '/inc/class-mailer.php');
<?php

namespace WP_SMTP\Inc;

use PHPMailer\PHPMailer\PHPMailer;

class Mailer 
{
    /**
     * Constructor
     */
    public function __construct()
    {
        // Bail if ACF isn't installed
        if(!function_exists('acf_add_options_page') ) {
            return;
        }

        // Hooks
        add_action('init', [$this, 'add_option_page']);
        add_action('phpmailer_init', [$this, 'set_smtp_settings']);
    }

    /**
     * Add option page
     */
    public function add_option_page()
    {
        acf_add_options_page([
            'page_title' => __('SMTP', 'turpoint-wp-smtp'),
            'autoload' => false,
            'menu_slug' => 'turpoint-wp-smtp-settings',
            'capability' => 'administrator',
            'parent_slug' => 'options-general.php',
        ]);


        // Add field group
        acf_add_local_field_group([
            'key' => 'group_turpoint_wp_smtp',
            'title' => __('SMTP', 'turpoint-wp-smtp'),
            'fields' => [
                [
                    'key' => 'field_turpoint_wp_smtp_host',
                    'label' => __('SMTP Host', 'turpoint-wp-smtp'),
                    'name' => 'turpoint_wp_smtp_host',
                    'type' => 'text',
                    'required' => 1,
                ],
                [
                    'key' => 'field_turpoint_wp_smtp_port',
                    'label' => __('SMTP Port', 'turpoint-wp-smtp'),
                    'name' => 'turpoint_wp_smtp_port',
                    'type' => 'text',
                    'required' => 1,
                ],
                [
                    'key' => 'field_turpoint_wp_smtp_username',
                    'label' => __('SMTP Username', 'turpoint-wp-smtp'),
                    'name' => 'turpoint_wp_smtp_username',
                    'type' => 'text',
                    'required' => 1,
                ],
                [
                    'key' => 'field_turpoint_wp_smtp_password',
                    'label' => __('SMTP Password', 'turpoint-wp-smtp'),
                    'name' => 'turpoint_wp_smtp_password',
                    'type' => 'text',
                    'required' => 1,
                ],
                [
                    'key' => 'field_turpoint_wp_smtp_from_email',
                    'label' => __('SMTP From - Email', 'turpoint-wp-smtp'),
                    'name' => 'turpoint_wp_smtp_from_email',
                    'type' => 'email',
                    'required' => 1,
                ],
                [
                    'key' => 'field_turpoint_wp_smtp_from_name',
                    'label' => __('SMTP From - Name', 'turpoint-wp-smtp'),
                    'name' => 'turpoint_wp_smtp_from_name',
                    'type' => 'text',
                    'required' => 1,
                ],
            ],
            'location' => [
                [
                    [
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'turpoint-wp-smtp-settings',
                    ],
                ],
            ],
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'active' => true,
        ]);
    }

    /**
     * Set SMTP settings
     */
    public function set_smtp_settings(PHPMailer $phpmailer)
    {
        $phpmailer->Host = get_field('turpoint_wp_smtp_host', 'options');
        $phpmailer->Port = get_field('turpoint_wp_smtp_port', 'options');
        $phpmailer->SMTPAuth = true;
        $phpmailer->Username = get_field('turpoint_wp_smtp_username', 'options');;
        $phpmailer->Password = get_field('turpoint_wp_smtp_password', 'options');;
        $phpmailer->setFrom(get_field('turpoint_wp_smtp_from_email', 'options'), get_field('turpoint_wp_smtp_from_name', 'options'));
        $phpmailer->isSMTP();
    }
}

/**
 * Callback
 */
$mailer = new Mailer();
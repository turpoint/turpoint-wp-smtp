# WP SMTP

A plugin for sending all e-mails of a WordPress installation via an external SMTP account. This plugin requires ACF to be installed.

## Installation via composer

Add a reference to this repository in the `repositories` section of your projects `composer.json`:

```
"repositories" : {
    ...
    {
        "type": "vcs",
        "url": "https://gitlab.com/turpoint/turpoint-wp-smtp.git"
    },
    ...
}
```

And then require the package:

```
composer require turpoint/turpoint-wp-smtp
```